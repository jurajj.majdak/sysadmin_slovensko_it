#!/bin/bash
source .env
#status=$(curl --location --request GET 'https://status.digitalocean.com/api/v2/status.json')
status='{"page":{"id":"s2k7tnzlhrpw","name":"DigitalOcean","url":"http://status.digitalocean.com","updated_at": "2022-06-08T14:36:34Z"},"status": {"description": "Partial System Outage","indicator": "major"}}'

status_cut=$(cut -d } -f 2 <<< $status | sed 's/{//g')
#echo $status_cut

printf "Subject:status${status}"  | msmtp -a gmail $SKIT_ALERT_EMAIL

