import json, os, smtplib
from urllib.request import urlopen, Request
from dotenv import load_dotenv


load_dotenv()
SKIT_ALERT_EMAIL= os.getenv("SKIT_ALERT_EMAIL")
SMTP = os.getenv("SMTP")
url = "https://status.digitalocean.com/api/v2/status.json"
httprequest = Request(url, headers={"Accept": "application/json"})
sender = "daah@daah.com" #add sener email

with urlopen(httprequest) as response: #reads API and stores it
    #print(response.status)
    json_data = json.loads(response.read()) 

#print (json_data["status"])

smtpObj = smtplib.SMTP (SMTP)  #add your SMTP server addres
smtpObj.sendmail(sender, SKIT_ALERT_EMAIL, json_data["status"] )  


 
